/**
 * Created by CaMi on 11 Nov 2018.
 */

const mongoLocalUri = 'mongodb://localhost/pregnancy ';
const mongoLabUri='mongodb://root:cami22@ds024778.mlab.com:24778/pregnancy';
const jwtPrivateKey='jwt-private-key';
module.exports = {
    mongoLocalUri: mongoLocalUri,
    mongoLabUri:mongoLabUri,
    jwtPrivateKey:jwtPrivateKey
};