const Joi = require('joi');
const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, email: true, unique: true, maxlength: 255},
    password: {type: String, required: true},
    createdAt: {type: Date, default: Date.now}
});
userSchema.methods.generateAuthToken = function () {
    return jwt.sign({_id: this._id, name: this.name, email: this.email}, 'jwt-private-key');
};
const User = mongoose.model('User', userSchema);

const validateUser = user => {
    const schema = {
        name: Joi.string().max(50).required(),
        email: Joi.string().max(50).required().email(),
        password: Joi.string().max(50).required()
    };
    return Joi.validate(user, schema);
};

module.exports = {
    User: User,
    validate: validateUser
};
