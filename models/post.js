const mongoose = require('mongoose');
const Joi = require('joi');

const postSchema = mongoose.Schema({
    title: {type: String, required: true},
    content: {type: String, required: true},
    publishedAt: {type: String},
    createdAt: {type: Date, default: Date.now}
});

const Post = mongoose.model('Post', postSchema);
const validatePost = post => {
    const schema = {
        title: Joi.string().max(50).required(),
        content: Joi.string().required()
    };
    return Joi.validate(post, schema);
};
module.exports = {
    Post: Post,
    validate: validatePost
};
