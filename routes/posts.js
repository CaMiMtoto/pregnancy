const express = require('express');
const _ = require('lodash');
const router = express.Router();
const {Post, validate} = require('../models/post');


router.get('/', async (req, res) => {
    let posts = await Post.find();
    res.status(200).json(posts);
});

router.post('/', async (req, res) => {
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);


    let post = new Post(_.pick(req.body, ['title', 'content']));
    try {
        const result = await post.save();
        return res.status(201).send(result);
    } catch (e) {
        return res.status(500).send("Unable to save try again");
    }
});

module.exports = router;