const express = require('express');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const Joi = require('joi');
const router = express.Router();
const {User, validate} = require('../models/user');
const auth = require('../middleware/auth');

router.get('/', async (req, res) => {
    let users = await User.find()
        .select('-password');
    res.status(200).json(users);
});
// logged in user
router.get('/me', auth, async (req, res) => {
    let user = await User.findById(req.user._id)
        .select('-password');
    res.status(200).json(user);
});

router.post('/register', async (req, res) => {
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({email: req.body.email});
    if (user) return res.status(400).send("User already registered.");

    user = new User(_.pick(req.body, ['name', 'email', 'password']));
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    try {
        await user.save();
        const result = _.pick(user, ['_id', 'name', 'email', 'createdAt']);
        const token = user.generateAuthToken();
        return res.header('x-auth-token', token).status(201).send(result);
    } catch (e) {
        return res.status(500).send("Unable to save try again");
    }
});

router.post('/login', async (req, res) => {
    const {error} = validateLogin(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({email: req.body.email});
    if (!user) return res.status(400).send("Invalid email or password.");
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).send("Invalid email or password.");
    const token = user.generateAuthToken();
    res.status(200).send(token);
});


const validateLogin = user => {
    const schema = {
        email: Joi.string().max(50).required().email(),
        password: Joi.string().max(50).required()
    };
    return Joi.validate(user, schema);
};

module.exports = router;
